//
//  AppConfig.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol AppConfigProtocol {
  // MARK: Backend

  /// Backend server URL
  var baseUrl: String { get }

  /// API URL
  var apiUrl: String { get }
  var apiVersion: String { get }

  /// URL string for the privacy policy  page for the app
  var privacyPolicyUrl: String { get }
  /// URL string for the terms of service page for the app
  var termsOfServiceUrl: String { get }

  // MARK: Defaults

  /// Default pagination items per page
  var defaultPageSize: Int { get }
  /// Default compression quality for UIImage before sending to backend
  var defaultPhotoCompression: Float { get }
  /// Default debounce timer in seconds for resending of code to email and mobile number
  var defaultCodeResendTimerInSeconds: Float { get }

  // MARK: Password

  var minPasswordLength: Int { get }
  var maxPasswordLength: Int { get }

  // MARK: Verification

  /// If email verification is skippable
  var emailVerificationSkippable: Bool { get }

  // MARK: Others

  var secrets: AppSecretsProtocol { get }
}

// MARK: - Default values

extension AppConfigProtocol {
  public var apiUrl: String { "\(baseUrl)/api" }
  public var apiVersion: String { "v1" }
  public var apiUrlWithVersion: String { "\(apiUrl)/\(apiVersion)" }

  public var privacyPolicyUrl: String { "\(apiUrlWithVersion)/pages/privacy" }
  public var termsOfServiceUrl: String { "\(apiUrlWithVersion)/pages/terms_of_service" }

  public var defaultPageSize: Int { 10 }
  public var defaultPhotoCompression: Float { 0.7 }
  public var defaultCodeResendTimerInSeconds: Float { 60 }

  public var minPasswordLength: Int { 8 }
  public var maxPasswordLength: Int { 32 }

  public var emailVerificationSkippable: Bool { true }
}

// MARK: - Concrete Type
   
public struct AppConfig: AppConfigProtocol {
  public var baseUrl: String { "https://api.baseplate.appetiserdev.tech" }

  public var secrets: AppSecretsProtocol { AppSecrets() }
}

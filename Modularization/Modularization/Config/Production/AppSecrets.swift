//
//  AppSecrets.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol AppSecretsProtocol {
  var googleClientID: String { get }
}

public struct AppSecrets: AppSecretsProtocol {
  public var googleClientID: String { "$(PROD_GOOGLE_CLIENT_ID)" }
}

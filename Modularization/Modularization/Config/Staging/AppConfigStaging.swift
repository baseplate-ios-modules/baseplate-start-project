//
//  AppConfigStaging.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct AppConfigStaging: AppConfigProtocol {
  public var baseUrl: String { "https://api.baseplate.appetiserdev.tech" }

  public var secrets: AppSecretsProtocol { AppSecretsStaging() }
}

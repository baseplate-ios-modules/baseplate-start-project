//
//  AppSecretsStaging.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct AppSecretsStaging: AppSecretsProtocol {
  public var googleClientID: String { "$(STAGING_GOOGLE_CLIENT_ID)" }
}

//
//  Array+Index.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public extension Array {
  subscript(safe idx: Int) -> Element? {
    idx < endIndex ? self[idx] : nil
  }
}

//
//  Bool+NumericValue.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreGraphics
import Foundation

public extension Bool {
  func intValue() -> Int {
    self ? 1 : 0
  }

  func floatValue() -> Float {
    Float(intValue())
  }

  func cgFloatValue() -> CGFloat {
    CGFloat(intValue())
  }
}

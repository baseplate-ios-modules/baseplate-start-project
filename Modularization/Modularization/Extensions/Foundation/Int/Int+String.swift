//
//  Int+String.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public extension Int {
  func twoDigitString() -> String {
    return String(format: "%02d", self)
  }
}

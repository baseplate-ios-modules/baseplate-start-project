//
//  NSMutableAttributedString+Utils.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public extension NSMutableAttributedString {
  func attributedString(
    _ string: String?,
    attributes: [NSAttributedString.Key: Any]? = nil
  ) -> NSMutableAttributedString {
    guard
      let text = string,
      text.count > 0,
      let range = self.string.range(of: text)
    else { return self }

    let attrString = NSMutableAttributedString(attributedString: self)
    attrString.setAttributes(
      attributes,
      range: NSRange(range, in: text)
    )
    return attrString
  }

  func attributedString(
    _ string: String?,
    font: UIFont,
    foregroundColor: UIColor
  ) -> NSMutableAttributedString {
    return attributedString(
      string,
      attributes: [
        .foregroundColor: foregroundColor,
        .font: font
      ]
    )
  }
}

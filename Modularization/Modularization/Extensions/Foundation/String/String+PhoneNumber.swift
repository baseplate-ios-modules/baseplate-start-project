//
//  String+PhoneNumber.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import PhoneNumberKit

public extension String {
  var formattedPhoneNumber: String? {
    do {
      let phoneNumberKit = PhoneNumberKit()
      let phoneNumber = try phoneNumberKit.parse(self)
      return phoneNumberKit.format(
        phoneNumber,
        toType: .international
      )
    } catch {
      debugPrint(error.localizedDescription)
      return nil
    }
  }
}

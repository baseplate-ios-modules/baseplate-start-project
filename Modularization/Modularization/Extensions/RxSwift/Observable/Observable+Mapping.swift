//
//  Observable+Mapping.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import RxSwift

public extension Observable {
  func mapToVoid() -> Observable<Void> {
    return map(to: Void())
  }
  
  func map<T>(to value: T) -> Observable<T> {
    return map { _ in value }
  }
}

//
//  UIAlertAction+Static.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public extension UIAlertAction {
  static func cancelAction(with title: String = S.cancel()) -> UIAlertAction {
    UIAlertAction(
      title: title,
      style: .cancel
    )
  }
}

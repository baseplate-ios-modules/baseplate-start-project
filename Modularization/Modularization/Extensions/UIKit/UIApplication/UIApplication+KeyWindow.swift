//
//  UIApplication+KeyWindow.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public extension UIApplication {
  var multisceneKeyWindow: UIWindow? {
    windows.filter { $0.isKeyWindow }.first
  }
}

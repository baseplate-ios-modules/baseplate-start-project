//
//  UICollectionView+Rx.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import RxCocoa
import RxSwift

public extension Reactive where Base: UICollectionView {
  var contentSize: ControlEvent<CGSize> {
    let source: Observable<CGSize> = base.rx
      .observe(CGSize.self, "contentSize")
      .compactMap { $0 }
      .distinctUntilChanged()
    return ControlEvent(events: source)
  }
}

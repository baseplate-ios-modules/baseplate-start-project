//
//  UICollectionViewCell+Static.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public extension UICollectionViewCell {
  static var reuseIdentifier: String {
    String(describing: self)
  }
}

//
//  UIImage+Data.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

public extension UIImage {
  var looselyCompressedJpegData: Data? {
    jpegData(compressionQuality: 1)
  }
}

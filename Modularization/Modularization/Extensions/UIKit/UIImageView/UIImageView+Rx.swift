//
//  UIImageView+Rx.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import RxCocoa
import RxSwift

public extension UIImageView {
  func bindHiddenIfNilImage() {
    rx.observe(UIImage.self, "image")
      .map { $0 == nil }
      .bind(to: rx.isHidden)
      .disposed(by: rx.disposeBag)
  }
}

//
//  TypeAliases.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

// MARK: - Typealiases

// Empty Result + Void Return
public typealias EmptyResult<ReturnType> = () -> ReturnType

// Custom Result + Custom Return
public typealias SingleResultWithReturn<T, ReturnType> = ((T) -> ReturnType)
public typealias DoubleResultWithReturn<T1, T2, ReturnType> = ((T1, T2) -> ReturnType)
public typealias TripleResultWithReturn<T1, T2, T3, ReturnType> = ((T1, T2, T3) -> ReturnType)
// Max limit should be three arguments only

// Custom Result + Void Return
public typealias SingleResult<T> = SingleResultWithReturn<T, Void>
public typealias DoubleResult<T1, T2> = DoubleResultWithReturn<T1, T2, Void>
public typealias TripleResult<T1, T2, T3> = TripleResultWithReturn<T1, T2, T3, Void>
// Max limit should be three arguments only

// Common
public typealias VoidResult = EmptyResult<Void> // () -> Void
public typealias ErrorResult = SingleResult<Error> // (Error) -> Void
public typealias BoolResult = SingleResult<Bool> // (Bool) -> Void

// Optional. I think tuples with external parameter name is more readable
public typealias SingleTuple<T> = T
public typealias DoubleTuple<T1, T2> = (T1, T2)
public typealias TripleTuple<T1, T2, T3> = (T1, T2, T3)

// MARK: - Default Closures

public struct DefaultClosure {
  public static func voidResult() -> VoidResult { {} }
  public static func singleResult<T>() -> SingleResult<T> { { _ in } }
  public static func doubleResult<T1, T2>() -> DoubleResult<T1, T2> { { _, _ in } }
  public static func tripleResult<T1, T2, T3>() -> TripleResult<T1, T2, T3> { { _, _, _ in } }
}

//
//  DictionaryEncoder.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class DictionaryEncoder {
  private let encoder = JSONEncoder()

  public init() {}

  public var dateEncodingStrategy: JSONEncoder.DateEncodingStrategy {
    set { encoder.dateEncodingStrategy = newValue }
    get { return encoder.dateEncodingStrategy }
  }

  public var dataEncodingStrategy: JSONEncoder.DataEncodingStrategy {
    set { encoder.dataEncodingStrategy = newValue }
    get { return encoder.dataEncodingStrategy }
  }

  public var nonConformingFloatEncodingStrategy: JSONEncoder.NonConformingFloatEncodingStrategy {
    set { encoder.nonConformingFloatEncodingStrategy = newValue }
    get { return encoder.nonConformingFloatEncodingStrategy }
  }

  public var keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy {
    set { encoder.keyEncodingStrategy = newValue }
    get { return encoder.keyEncodingStrategy }
  }

  /// - parameter value: Any **Encodable** conforming value.
  /// - parameter compact: If True, removes keyval pairs with nil values.
  public func encode<T>(_ value: T, compact: Bool = true) throws -> [String: Any] where T: Encodable {
    let data = try encoder.encode(value)
    let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
    return compact ? dictionary.filter { !($0.value is NSNull) } : dictionary
  }
}

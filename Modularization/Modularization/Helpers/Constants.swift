//
//  Constants.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: Any]

// swiftlint:disable:next type_name
public typealias S = R.string.localizable

public struct Constants {
  public struct Formatters {
    public static let debugConsoleDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
      formatter.timeZone = .utc
      return formatter
    }()
    
    public static let birthdateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
      formatter.timeZone = .utc
      return formatter
    }()
  }
}

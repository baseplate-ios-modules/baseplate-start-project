//
//  DirtyFormChecker.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class DirtyFormChecker: DirtyFormCheckerProtocol {
  private let views: [UIView]
  private var initialHash: Int?

  public init(views: [UIView]) {
    self.views = views
  }

  public func resetHash() {
    initialHash = generateHash()
  }

  public func isFormDirty() -> Bool {
    let newHash = generateHash()
    return newHash != initialHash
  }
}

// MARK: - Utils

private extension DirtyFormChecker {
  func generateHash() -> Int {
    var hasher = Hasher()

    views
      .forEach { view in
        if let textField = view as? UITextField {
          hasher.combine(textField.text)
        } else if let textView = view as? UITextView {
          hasher.combine(textView.text)
        } else if let imageView = view as? UIImageView {
          hasher.combine(imageView.image)
        }
      }

    return hasher.finalize()
  }
}

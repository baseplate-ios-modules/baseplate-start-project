//
//  DirtyFormCheckerProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol DirtyFormCheckerProtocol {
  func resetHash()
  func isFormDirty() -> Bool
}

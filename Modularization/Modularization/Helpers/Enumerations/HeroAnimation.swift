//
//  HeroAnimation.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public enum HeroAnimation: String {
  case createPost
  case postAuthorView
  case postView
}

// MARK: - Helpers

public extension HeroAnimation {
  var id: String { rawValue }

  func createID(withKey key: String) -> String {
    "\(key) \(rawValue)"
  }
}

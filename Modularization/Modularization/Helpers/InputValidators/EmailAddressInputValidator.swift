//
//  EmailAddressInputValidator.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct EmailAddressInputValidator: InputValidator {
  public static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let email = inputs, !email.isEmpty else {
      return .failure(.requiredEmail)
    }

    guard InputValidatorUtil.isValidEmail(email) else {
      return .failure(.invalidEmail)
    }

    let validInputs = ValidInputs(email)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

public extension EmailAddressInputValidator {
  typealias Inputs = String?
  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredEmail, invalidEmail

    public var errorDescription: String? {
      switch self {
      case .requiredEmail:
        return S.emailFieldErrorMissingValue()
      case .invalidEmail:
        return S.emailFieldErrorInvalidValue()
      }
    }
  }
}

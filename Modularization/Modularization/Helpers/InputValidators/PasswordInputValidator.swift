//
//  PasswordInputValidator.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct PasswordInputValidator: InputValidator {
  public static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let password = inputs, !password.isEmpty else {
      return .failure(.requiredPassword)
    }

    let validInputs = ValidInputs(password)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

public extension PasswordInputValidator {
  typealias Inputs = String?
  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredPassword

    public var errorDescription: String? {
      switch self {
      case .requiredPassword:
        return S.passwordFieldErrorMissingValue()
      }
    }
  }
}

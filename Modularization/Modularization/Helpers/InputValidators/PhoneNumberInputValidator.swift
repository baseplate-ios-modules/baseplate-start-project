//
//  PhoneNumberInputValidator.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct PhoneNumberInputValidator: InputValidator {
  public static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let phoneNumber = inputs, !phoneNumber.isEmpty else {
      return .failure(.requiredPhone)
    }

    let validInputs = ValidInputs(phoneNumber)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

public extension PhoneNumberInputValidator {
  typealias Inputs = String?

  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredPhone

    public var errorDescription: String? {
      switch self {
      case .requiredPhone:
        return S.phoneFieldErrorMissingValue()
      }
    }
  }
}

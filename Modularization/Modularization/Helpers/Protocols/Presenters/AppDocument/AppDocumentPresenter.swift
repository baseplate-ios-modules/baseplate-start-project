//
//  AppDocumentPresenter.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class AppDocumentPresenter: AppDocumentPresenterProtocol, WebPagePresenterProtocol {
  private let config: AppConfigProtocol

  public init(config: AppConfigProtocol = App.shared.config) {
    self.config = config
  }
}

// MARK: - Methods

public extension AppDocumentPresenter {
  func presentTermsOfServicePage(
    fromController controller: UIViewController
  ) {
    let metadata = WebPageMetadata(
      title: S.termsCondition(),
      urlString: App.shared.config.termsOfServiceUrl
    )

    presentWebPage(
      withMetadata: metadata,
      fromController: controller
    )
  }

  func presentPrivacyPolicyPage(
    fromController controller: UIViewController
  ) {
    let metadata = WebPageMetadata(
      title: S.privacyPolicy(),
      urlString: App.shared.config.privacyPolicyUrl
    )

    presentWebPage(
      withMetadata: metadata,
      fromController: controller
    )
  }
}

//
//  DialogOption.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct DialogOption {
  public let title: String
  public var onSelect: VoidResult = DefaultClosure.voidResult()
  public var isPreferred: Bool = false
}

// MARK: - Equatable

extension DialogOption: Equatable {
  public static func == (lhs: DialogOption, rhs: DialogOption) -> Bool {
    lhs.title == rhs.title
  }
}

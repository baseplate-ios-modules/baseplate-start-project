//
//  InfoAction.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct InfoAction {
  public let title: String
  public let onSelect: VoidResult
}

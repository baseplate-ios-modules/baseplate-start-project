//
//  InfoProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public protocol InfoProtocol {
  var message: String { get }
  var foregroundColor: UIColor { get }
  var backgroundColor: UIColor { get }
  var duration: TimeInterval { get }
  var action: InfoAction? { get }
}

public extension InfoProtocol {
  var duration: TimeInterval { 3 }
  var action: InfoAction? { nil }
}

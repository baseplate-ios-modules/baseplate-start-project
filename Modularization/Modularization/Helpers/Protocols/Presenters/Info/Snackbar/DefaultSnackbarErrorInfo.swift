//
//  DefaultSnackbarErrorInfo.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public struct DefaultSnackbarErrorInfo: InfoProtocol {
  public let errorMessage: String

  public init(errorMessage: String) {
    self.errorMessage = errorMessage
  }

  public init(error: Error) {
    self.init(errorMessage: error.localizedDescription)
  }
}

public extension DefaultSnackbarErrorInfo {
  var message: String { errorMessage }
  var foregroundColor: UIColor { T.component.snackbar.error.foregroundColor }
  var backgroundColor: UIColor { T.component.snackbar.error.backgroundColor }
  var action: InfoAction? { .snackbarDismiss }
}

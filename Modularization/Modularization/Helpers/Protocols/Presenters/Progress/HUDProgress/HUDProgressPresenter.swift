//
//  HUDProgressPresenter.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import SVProgressHUD

public class HUDProgressPresenter: ProgressPresenterProtocol {
  public func presentIndefiniteProgress(
    from source: UIViewController
  ) {
    presentIndefiniteProgress(
      message: nil,
      from: source
    )
  }

  public func presentIndefiniteProgress(
    message: String?,
    from source: UIViewController
  ) {
    SVProgressHUD.show(
      message: message,
      mask: .black
    )
  }

  public func presentProgress(
    value: Float,
    from source: UIViewController
  ) {
    presentProgress(
      value: value,
      message: nil,
      from: source
    )
  }

  public func presentProgress(
    value: Float,
    message: String?,
    from source: UIViewController
  ) {
    SVProgressHUD.showProgress(
      Float(value),
      status: message
    )
  }

  public func dismiss() {
    dismiss(onComplete: DefaultClosure.voidResult())
  }

  public func dismiss(
    onComplete: @escaping VoidResult
  ) {
    SVProgressHUD.dismiss(completion: onComplete)
  }
}

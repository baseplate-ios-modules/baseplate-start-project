//
//  ProgressPresenterProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public protocol ProgressPresenterProtocol {
  func presentIndefiniteProgress(
    from source: UIViewController
  )

  func presentIndefiniteProgress(
    message: String?,
    from source: UIViewController
  )

  func presentProgress(
    value: Float,
    from source: UIViewController
  )

  func presentProgress(
    value: Float,
    message: String?,
    from source: UIViewController
  )

  func dismiss()

  func dismiss(
    onComplete: @escaping VoidResult
  )
}

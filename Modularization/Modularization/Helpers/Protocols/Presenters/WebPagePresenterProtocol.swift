//
//  WebPagePresenter.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public protocol WebPagePresenterProtocol {
  func presentWebPage(
    withMetadata metadata: WebPageMetadata,
    fromController controller: UIViewController
  )
}

// MARK: - Defaults

public extension WebPagePresenterProtocol {
  func presentWebPage(
    withMetadata metadata: WebPageMetadata,
    fromController controller: UIViewController
  ) {
    let wvc = WebViewController()
    wvc.title = metadata.title
    wvc.urlString = metadata.urlString

    let nc = NavigationController(rootViewController: wvc)
    controller.present(nc, animated: true)
  }
}

// MARK: - Types

public typealias WebPageMetadata = (
  title: String,
  urlString: String
)

//
//  Attributeable.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public protocol Attributeable where Self: UIView {
  associatedtype AttributeType: ViewAttributeType
  func applyAttribute(_ attribute: AttributeType)
}

public protocol ViewAttributeType {
  associatedtype ViewType: UIView
  func apply(to view: ViewType)
}

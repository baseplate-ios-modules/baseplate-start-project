//
//  LogoutTriggerViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class LogoutTriggerViewModel: LogoutTriggerViewModelProtocol {
  
  private let service: LogoutServiceProtocol
  
  public init(service: LogoutServiceProtocol = App.shared.logout) {
    self.service = service
  }
  
}

public extension LogoutTriggerViewModel {

  func logout() {
    service.logout(
      shouldBroadcast: false,
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
  
}

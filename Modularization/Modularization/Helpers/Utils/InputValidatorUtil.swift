//
//  InputValidatorUtil.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct InputValidatorUtil {
  /// Check whether the provided string is a valid Email address or not.
  public static func isValidEmail(_ email: String) -> Bool {
    let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
    return predicate.evaluate(with: email)
  }

  public static func isValidFirstName(_ firstName: String) -> Bool {
    return !firstName.isEmpty
  }

  public static func isValidLastName(_ lastName: String) -> Bool {
    return !lastName.isEmpty
  }

  public static func isValidPassword(
    _ password: String,
    minLength: Int,
    maxLenth: Int
  ) -> Bool {
    let regEx = "^.{\(minLength),\(maxLenth)}$"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
    return predicate.evaluate(with: password)
  }
}

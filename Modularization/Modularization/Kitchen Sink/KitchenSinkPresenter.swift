//
//  KitchenSinkPresenter.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public protocol KitchenSinkPresenter where Self: ViewController {}

extension KitchenSinkPresenter {
  public func setupKitchensinkTrigger(
    forView view: UIView,
    action: Selector
  ) {
    #if DEBUG
      let longPressRecognizer = UILongPressGestureRecognizer(
        target: self,
        action: action
      )
      view.addGestureRecognizer(longPressRecognizer)
    #endif
  }

  public func presentKitchenSinkController() {
    let vc = R.storyboard.kitchenSink.instantiateInitialViewController()!
    vc.modalPresentationStyle = .fullScreen
    present(vc, animated: true)
  }
}

//
//  AboutController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public class AboutController: ViewController {
  @IBOutlet private(set) var appIconContainerView: CustomView!
  @IBOutlet private(set) var appIconImageView: AppIconImageView!
  @IBOutlet private(set) var versionInfoView: VersionInfoView!

  private(set) var tableController: AboutTableController!

  // MARK: - Overrides

  public override func prepare(
    for segue: UIStoryboardSegue,
    sender: Any?
  ) {
    if let tableController = segue.destination as? AboutTableController {
      self.tableController = tableController
    }
  }
}

// MARK: - Lifecycle

extension AboutController {
  public override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension AboutController {
  func setup() {
    setupAppIcon()
    setupVersionInfo()
  }

  func setupAppIcon() {
    appIconContainerView.layer.masksToBounds = false
    appIconContainerView.applyShadow(
      color: .black,
      opacity: 0.12,
      offSet: CGSize(width: 0, height: 3),
      radius: 10
    )

    appIconImageView.cornerRadius = 12
  }

  func setupVersionInfo() {
    versionInfoView.versionLabel.textColor = R.color.textOnLightSecondary()
  }
}

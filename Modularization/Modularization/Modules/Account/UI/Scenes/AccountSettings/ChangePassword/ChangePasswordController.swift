//
//  ChangePasswordController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

public class ChangePasswordController: ScrollViewController {
  var viewModel: ChangePasswordViewModelProtocol!
  
  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet public private(set) var continueButton: MDCButton!

  public private(set) var fieldInputController: MDCTextInputControllerBase!
  
  public override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension ChangePasswordController {
  public override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    passwordField.becomeFirstResponder()
  }

  public override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension ChangePasswordController {
  func setup() {
    setupTitleAndMessage()
    setupPasswordField()

    continueButton.applyStyle(.primary)
  }
  
  func setupTitleAndMessage() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
  }

  func setupPasswordField() {
    fieldInputController = MDCHelper.inputController(for: passwordField)
  }
}

// MARK: - Bind

private extension ChangePasswordController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension ChangePasswordController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.changePassword(
      with: passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - SingleFormInputControllerProtocol

extension ChangePasswordController: SingleFormInputControllerProtocol {
  public var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }
  public var field: MDCTextField! { passwordField }
}

//
//  VerifyChangeCredentialController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

public class VerifyChangeCredentialController: ScrollViewController {
  var viewModel: VerifyChangeCredentialViewModelProtocol!

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet public private(set) var resendButtonView: DebouncedButtonView!

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  @IBOutlet public private(set) var textFields: [VerificationCodeField]!

  private(set) var inputControllers: [MDCTextInputControllerUnderline] = []

  public override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension VerifyChangeCredentialController {
  public override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textFields.first!.becomeFirstResponder()
  }

  public override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension VerifyChangeCredentialController {
  func setup() {
    setupLabels()
    setupFields()
    setupFieldInputControllers()
    setupResendButtonView()
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension VerifyChangeCredentialController {
  func bind() {
    bindFields()
    bindResendButtonView()
  }
}

// MARK: - Handlers

private extension VerifyChangeCredentialController {
  func handleVerificationSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
    }
  }

  func handleVerificationError() -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.infoPresenter.presentErrorInfo(error: error)
      self.clearFields(makeFirstFieldFirstResponder: true)
    }
  }

  func handleResendCodeSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.infoPresenter.presentSuccessInfo(message: S.accountVerifAlertsCodeSent())
      self.clearFields(makeFirstFieldFirstResponder: true)
    }
  }
}

// MARK: - Routing

private extension VerifyChangeCredentialController {
  func presentNextPage() {
    let vc = R.storyboard.authSignup.onboardingNameController()!
    vc.viewModel = OnboardingNameViewModel()
    vc.logoutTriggerVM = LogoutTriggerViewModel()
    vc.inputCache = OnboardingInputCache()
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - CodeVerificationControllerProtocol

extension VerifyChangeCredentialController: CodeVerificationControllerProtocol {
  public var codeVerificationVM: CodeVerificationViewModelProtocol { viewModel }
  
  public func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  public func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let self = self else { return }
      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.verify(
        using: code,
        onSuccess: self.handleSuccess(),
        onError: self.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension VerifyChangeCredentialController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension VerifyChangeCredentialController: UITextFieldDelegate {
  public func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}

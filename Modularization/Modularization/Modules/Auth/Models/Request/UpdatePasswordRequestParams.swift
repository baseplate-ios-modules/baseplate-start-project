//
//  UpdatePasswordRequestParams.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct UpdatePasswordRequestParams: APIRequestParameters {
  /// The user's username
  public var username: String

  /// The token sent to user's email when he requested for password reset.
  public var token: String

  /// The new password.
  public var password: String
  public var passwordConfirmation: String
}

extension UpdatePasswordRequestParams: Codable {
  public enum CodingKeys: String, CodingKey {
    case username, token, password
    case passwordConfirmation = "password_confirmation"
  }
}

extension UpdatePasswordRequestParams: Equatable {}

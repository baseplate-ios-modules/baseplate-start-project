//
//  UserAuthResponse.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

/// Model representation of Login and Registration endpoint responses.
///
/// Sample data:
///
/// ```
///   {
///     "data": {
///       "access_token": "eyJ0eXAiOiJKV1QiLC...QmSh3zY",
///       "token_type": "bearer",
///       "expires_in": 2073600,
///       "user": {...}
///   }
/// ```
///
public struct UserAuthResponse: APIModel, Codable {
  public let user: User
  public let accessToken: String
  public let tokenType: String
  public let expiresIn: String
}

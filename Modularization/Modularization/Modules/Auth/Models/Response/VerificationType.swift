//
//  User.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

public enum VerificationType: String, Codable {
  case email
  case phone = "phone_number"
}

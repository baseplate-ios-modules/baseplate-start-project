//
//  SocialAuthServiceProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol LoginServiceProtocol: AuthServiceProtocol {
  func loginWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func loginWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

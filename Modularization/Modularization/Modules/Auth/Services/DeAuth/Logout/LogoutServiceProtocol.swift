//
//  LogoutServiceProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol LogoutServiceProtocol: DeAuthServiceProtocol {
  func logout(
    shouldBroadcast: Bool,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

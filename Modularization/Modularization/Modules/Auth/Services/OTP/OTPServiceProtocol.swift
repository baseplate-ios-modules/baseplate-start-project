//
//  OTPServiceProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

public protocol OTPServiceProtocol {
  func generateOTP(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

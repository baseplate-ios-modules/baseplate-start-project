//
//  AccountVerificationServiceProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol AccountVerificationServiceProtocol: UserServiceProtocol {
  func verify(
    for type: VerificationType,
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func resendCode(
    for type: VerificationType,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

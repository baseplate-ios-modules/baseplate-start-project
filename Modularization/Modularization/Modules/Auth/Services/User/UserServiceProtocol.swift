//
//  UserServiceProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol UserServiceProtocol {
  var onUserResult: SingleResult<User>? { get set }
  var onAvatarResult: SingleResult<Photo>? { get set }
}

public extension UserServiceProtocol {
  func handleUserResult(thenExecute handler: @escaping VoidResult) -> SingleResult<User> {
    return { [onUserResult] user in
      onUserResult?(user)
      handler()
    }
  }
  
  func handleAvatarResult(thenExecute handler: @escaping VoidResult) -> SingleResult<Photo> {
    return { [onAvatarResult] avatar in
      onAvatarResult?(avatar)
      handler()
    }
  }
}

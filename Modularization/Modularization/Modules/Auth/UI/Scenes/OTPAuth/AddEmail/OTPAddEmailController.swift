//
//  OTPAddEmailController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields

public class OTPAddEmailController: ScrollViewController {
  var viewModel: OTPAddEmailViewModelProtocol!
  public var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }
  var inputCache: OnboardingInputCacheProtocol! = OnboardingInputCache()

  @IBOutlet public private(set) var field: MDCTextField!
  @IBOutlet public private(set) var continueButton: MDCButton!
  @IBOutlet public private(set) var skipButton: UIBarButtonItem!
  
  public private(set) var fieldInputController: MDCTextInputControllerBase!

  public override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension OTPAddEmailController {
  public override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  public override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    view.endEditing(true)
  }
}

// MARK: - Setup

private extension OTPAddEmailController {
  func setup() {
    setupNavBar()
    setupEmailField()

    continueButton.applyStyle(.primary)
  }
  
  func setupNavBar() {
    // Always hide back button
    navigationItem.leftBarButtonItem = nil
  }

  func setupEmailField() {
    field.applyAttribute(.email)
    field.placeholder = S.emailFieldPlaceholder()
    field.returnKeyType = .continue
    field.enablesReturnKeyAutomatically = true

    fieldInputController = MDCHelper.inputController(for: field)

    field.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension OTPAddEmailController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension OTPAddEmailController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submit(
      email: field.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }

  @IBAction
  func skipButtonTapped(_ sender: AnyObject) {
    viewModel.skip()
  }
}

// MARK: - SingleFormInputControllerProtocol

extension OTPAddEmailController: SingleFormInputControllerProtocol {}

// MARK: - UsernameCheckerControllerProtocol

//extension OTPAddEmailController: UsernameCheckerControllerProtocol {}

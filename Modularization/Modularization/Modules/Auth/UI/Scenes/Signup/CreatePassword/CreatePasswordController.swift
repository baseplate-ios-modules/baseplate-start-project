//
//  CreatePasswordController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

public class CreatePasswordController: ViewController {
  var viewModel: CreatePasswordViewModelProtocol!
  public var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet public private(set) var scrollContentView: UIView!
  @IBOutlet public private(set) var accountLabel: UILabel!

  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet public private(set) var continueButton: MDCButton!

  public private(set) var fieldInputController: MDCTextInputControllerBase!
  
  public override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension CreatePasswordController {
  public override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  public override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension CreatePasswordController {
  func setup() {
    setupAccountLabel()
    setupPasswordField()

    continueButton.applyStyle(.primary)
  }

  func setupAccountLabel() {
    accountLabel.text = viewModel.usernameText
  }

  func setupPasswordField() {
    passwordField.returnKeyType = .continue
    passwordField.delegate = self

    fieldInputController = MDCHelper.inputController(for: passwordField)
    passwordField.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension CreatePasswordController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

extension CreatePasswordController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.register(
      with: passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - UITextFieldDelegate

extension CreatePasswordController: UITextFieldDelegate {
  public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    guard !textField.text.isNilOrEmpty else { return false }

    if textField == passwordField {
      continueButtonTapped(self)
    }

    return true
  }
}

// MARK: - SingleFormInputControllerProtocol

extension CreatePasswordController: SingleFormInputControllerProtocol {
  public var field: MDCTextField! { passwordField }
}

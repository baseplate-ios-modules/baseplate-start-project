//
//  APIPage.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct APIPage: Equatable {
  public let index: Int
  public let size: Int
}

// MARK: - Static

extension APIPage {
  public static var `default`: APIPage { .init(index: 1, size: App.shared.config.defaultPageSize) }
}

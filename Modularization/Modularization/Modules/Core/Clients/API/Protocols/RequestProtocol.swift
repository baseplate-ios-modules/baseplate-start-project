//
//  RequestProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Alamofire
import Foundation

public protocol RequestProtocol {
  func resume()
  func suspend()
  func cancel()
}

extension DataRequest: RequestProtocol {}

//
//  Model.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol Model {
  /// Returns a **JSONDecoder** instance that's configured for the conforming type.
  static func decoder() -> JSONDecoder

  /// Returns a **JSONEncoder** instance that's configured for the conforming type.
  static func encoder() -> JSONEncoder
}

// MARK: - Decodable

public extension Model where Self: Decodable {
  static func decoder() -> JSONDecoder {
    return JSONDecoder()
  }

  static func decode(_ data: Data) throws -> Self {
    return try decoder().decode(self, from: data)
  }

  static func decode(_ dictionary: [String: Any]) throws -> Self {
    return try decode(try JSONSerialization.data(withJSONObject: dictionary))
  }
}

// MARK: - Encodable

public extension Model where Self: Encodable {
  static func encoder() -> JSONEncoder {
    return JSONEncoder()
  }

  func encode() throws -> Data {
    return try Self.encoder().encode(self)
  }

  func dictionary() -> [String: Any]? {
    do {
      if let dict = try JSONSerialization.jsonObject(
        with: try encode(),
        options: .allowFragments
      ) as? [String: Any] {
        return dict.filter { !($0.value is NSNull) }
      }
    } catch {
      App.shared.errorHandling.processError(error)
    }
    return nil
  }

  func json() -> String? {
    do {
      return String(data: try encode(), encoding: .utf8)
    } catch {
      App.shared.errorHandling.processError(error)
      return nil
    }
  }
}

// MARK: - APIModel

public protocol APIModel: Model {}

public extension APIModel {
  static func decoder() -> JSONDecoder {
    // You can set your preferred decoding strategies here.
    let d = JSONDecoder()
    d.dateDecodingStrategy = .formatted(.iso8601)
    d.keyDecodingStrategy = .convertFromSnakeCase
    return d
  }

  static func encoder() -> JSONEncoder {
    // You can set your preferred encoding strategies here.
    let e = JSONEncoder()
    e.dateEncodingStrategy = .formatted(.iso8601)
    e.keyEncodingStrategy = .convertToSnakeCase
    return e
  }
}

/// Just a stand-in model for us to access the static **APIModel**
/// functions like `decoder()` and `encoder()`.
public struct GenericAPIModel: APIModel {}

// MARK: - APIRequestParameters

/// A Model type that is intended for parameterized API endpoint wrapper methods. Specifically,
/// those methods that have more than two non-Closure parameters.
public protocol APIRequestParameters: APIModel, Codable {}

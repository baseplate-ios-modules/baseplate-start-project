//
//  MDCTheme.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MaterialTextFields_ColorThemer
import MaterialComponents.MaterialTextFields_TypographyThemer

public struct MDCTheme {
  public let typographyScheme = MDCTypographyScheme()
  public let colorScheme = MDCSemanticColorScheme()
  public let containerScheme = MDCContainerScheme()

  public init() {
    setupTypographyScheme()
    setupColorScheme()

    setupContainerScheme()
  }
}

// MARK: - Setup

private extension MDCTheme {
  func setupTypographyScheme() {
    typographyScheme.headline1 = T.textStyle.largeTitle
    typographyScheme.subtitle1 = T.textStyle.subHeadline
    typographyScheme.caption = T.textStyle.caption
  }

  func setupColorScheme() {
    colorScheme.primaryColor = R.color.primaryFull()!
    colorScheme.errorColor = R.color.alertError()!
  }

  func setupContainerScheme() {
    containerScheme.typographyScheme = typographyScheme
    containerScheme.colorScheme = colorScheme
  }
}

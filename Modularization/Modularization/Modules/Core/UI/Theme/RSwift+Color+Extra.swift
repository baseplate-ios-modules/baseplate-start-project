//
//  RSwift+Color+Extra.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name

import UIKit

public extension R.color {
  enum form {}
}

// MARK: - Form

public extension R.color.form {
  enum textField {
    public static func placeholderTextColor() -> UIColor? { R.color.greysInactive() }
    public static func textColor() -> UIColor? { R.color.textOnLightRegular() }
  }
}

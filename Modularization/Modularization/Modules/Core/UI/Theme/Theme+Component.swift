//
//  Theme+Component.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name
// swiftlint:disable nesting

import Foundation
import UIKit

public extension Theme.component {
  enum loadBarButtonItem {
    public static let foregroundColor: UIColor = R.color.primaryFull()!
    public static let font: UIFont = T.font.semibold(ofSize: 17)
  }

  enum navBar {
    public static let barTintColor: UIColor = R.color.backgroundLight()!
    public static let font: UIFont = T.font.semibold(ofSize: 17)
    public static let tintColor: UIColor = R.color.textOnLightRegular()!
  }

  enum progressHUD {
    public static let backgroundColor: UIColor = R.color.greysLine()!
    public static let backgroundLayerColor: UIColor = R.color.greysLine()!
  }

  enum snackbar {
    public static let backgroundColor: UIColor = R.color.sheetDark()!
    public static let font: UIFont = T.font.regular(ofSize: 17)
    public static let foregroundColor: UIColor = R.color.textOnDarkRegular()!

    public enum error {
      public static let backgroundColor: UIColor = R.color.alertError()!
      public static let foregroundColor: UIColor = R.color.textOnPrimaryRegular()!
    }

    public enum success {
      public static let backgroundColor: UIColor = R.color.alertSuccess()!
      public static let foregroundColor: UIColor = R.color.textOnLightRegular()!
    }
  }

  enum viewController {
    public static let backgroundColor: UIColor = R.color.backgroundLight()!
  }

  enum window {
    public static let backgroundColor: UIColor = R.color.backgroundLight()!
    public static let tintColor: UIColor = R.color.primaryFull()!
  }
}

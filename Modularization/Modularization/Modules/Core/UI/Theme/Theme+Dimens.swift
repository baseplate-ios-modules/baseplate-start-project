//
//  Theme+Dimens.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name
// swiftlint:disable nesting

import Foundation
import UIKit

extension T.dimens {
  public enum cornerRadius {}
}

// MARK: - Corner Radius

public extension T.dimens.cornerRadius {
  static let button: CGFloat = 8
  static let card: CGFloat = 8
  static let sheet: CGFloat = 16
  static let textField: CGFloat = 8
}
 

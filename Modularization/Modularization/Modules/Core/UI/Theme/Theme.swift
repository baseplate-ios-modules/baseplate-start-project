//
//  Theme.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name

import UIKit

public typealias T = Theme

public enum Theme {
  public enum font {}
  public enum textStyle {}

  /// Holds styling variables for specific components. Mostly built-in components that's not included
  /// on the stylesheet document.
  public enum component {}

  /// Holds all measurement related stuff. `dimens` term is copied from Android
  public enum dimens {}
}

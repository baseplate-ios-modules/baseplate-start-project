//
//  NavigationController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public class NavigationController: UINavigationController {
  public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    if let controller = visibleViewController {
      return controller.supportedInterfaceOrientations
    }
    return super.supportedInterfaceOrientations
  }

  public override var shouldAutorotate: Bool {
    if let controller = visibleViewController {
      return controller.shouldAutorotate
    }
    return super.shouldAutorotate
  }

  public override var preferredStatusBarStyle: UIStatusBarStyle {
    if let controller = visibleViewController {
      return controller.preferredStatusBarStyle
    }
    return super.preferredStatusBarStyle
  }
}

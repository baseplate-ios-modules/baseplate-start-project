//
//  StaticTableViewController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class StaticTableViewController: UITableViewController, PresentersProviderProtocol {
  public lazy var progressPresenter: ProgressPresenterProtocol = HUDProgressPresenter()
  public lazy var dialogPresenter: DialogPresenterProtocol = AlertDialogPresenter()
  public lazy var infoPresenter: InfoPresenterProtocol = SnackbarInfoPresenter()

  public enum CloseButtonPosition {
    case left
    case right
  }

  /// Close button's position when this controller is presented modally. Defaults to `left`.
  public var preferredCloseButtonPosition: CloseButtonPosition { .left }

  public var shouldSetNavBarTransparent: Bool { false }

  public var viewWillAppearCallCount: Int { _viewWillAppearCallCount }

  private var _viewWillAppearCallCount = 0

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = T.component.viewController.backgroundColor
    setupNavBarItems()
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    _viewWillAppearCallCount += 1

    if shouldSetNavBarTransparent {
      navigationController?.navigationBar.setTransparent()
    }
  }

  public override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    if shouldSetNavBarTransparent {
      navigationController?.navigationBar.setNonTransparent()
    }
  }
}

// MARK: - Setup

private extension StaticTableViewController {
  /// A place for adding your custom navigation bar buttons.
  ///
  /// By default, a custom **Back** button will appear on the left side of the navigation bar
  /// if this controller isn't the root of its `navigationController`. On the other hand,
  /// if it's presented modally, we show a **Close** button.
  ///
  /// This method is called inside `viewDidLoad`.
  ///
  func setupNavBarItems() {
    if navigationController?.viewControllers.first != self {
      let backButton = UIBarButtonItem(
        image: R.image.arrowLeftBlack(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
      // TODO: Color should be from Theme/Styles
      backButton.tintColor = .black
      navigationItem.leftBarButtonItem = backButton
      navigationItem.leftItemsSupplementBackButton = false

      // Fix for bug where swiping from left to right to go back is gone.
      // The bug is the side effect for setting custom `leftBarButtonItem`.
      navigationController?.interactivePopGestureRecognizer?.delegate = self
    } else if isPresentedModally {
      let button = UIBarButtonItem(
        image: R.image.xClose(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
      if preferredCloseButtonPosition == .left {
        navigationItem.leftBarButtonItem = button
      } else {
        navigationItem.rightBarButtonItem = button
      }
    }
  }

  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    if navigationController?.viewControllers.first != self {
      navigationController?.popViewController(animated: true)
    } else if isPresentedModally {
      dismiss(animated: true, completion: nil)
    }
  }
}

// MARK: - UIGestureRecognizerDelegate

extension StaticTableViewController: UIGestureRecognizerDelegate {
  public func gestureRecognizer(
    _ gestureRecognizer: UIGestureRecognizer,
    shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer
  ) -> Bool {
    return true
  }
}

// MARK: - HandlersProviderProtocol

extension StaticTableViewController: HandlersProviderProtocol {}

//
//  APButton.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MDCButton

@IBDesignable
public class APButton: MDCButton {
  public var buttonStyle: MDCButtonStyleType { .primary }

  public override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  public override func prepareForInterfaceBuilder() {
    setup()
  }

  private func setup() {
    applyStyle(buttonStyle)
  }
}

// MARK: - APCustomButton

public class APCustomButton: APButton {
  @IBInspectable
  public var foregroundColor: UIColor = R.color.textOnLightRegular()! {
    didSet { didSetTitleColor() }
  }

  @IBInspectable
  public var backgroundColour: UIColor = R.color.primaryFull()! {
    didSet { didSetBackgroundColor() }
  }

  @IBInspectable
  public var rippleColor: UIColor = R.color.textOnLightRegular()! {
    didSet { didSetRipple() }
  }

  @IBInspectable
  public var rippleColorOpacity: CGFloat = 0.1 {
    didSet { didSetRipple() }
  }

  @IBInspectable
  public var outlineColor: UIColor? = nil {
    didSet { didSetOutline() }
  }

  @IBInspectable
  public var outlineThickness: CGFloat = 0 {
    didSet { didSetOutline() }
  }

  public override var buttonStyle: MDCButtonStyleType { .textButton }

  public override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    didSetTitleColor()
    didSetBackgroundColor()
    didSetRipple()
    didSetOutline()
  }

  private func didSetTitleColor() {
    setTitleColor(foregroundColor, for: .normal)
  }

  private func didSetBackgroundColor() {
    setBackgroundColor(backgroundColour, for: .normal)
  }

  private func didSetRipple() {
    inkColor = rippleColor.withAlphaComponent(rippleColorOpacity)
  }

  private func didSetOutline() {
    let thickness = (outlineColor != nil) ? outlineThickness : 0
    setBorderWidth(thickness, for: .normal)
    setBorderColor(outlineColor, for: .normal)
  }
}

// MARK: - APButtonPrimary

public class APButtonPrimary: APButton {}

// MARK: - APButtonSecondary

public class APButtonSecondary: APButton {
  public override var buttonStyle: MDCButtonStyleType { .secondary }
}

// MARK: - APButtonOutlined

public class APButtonOutlined: APButton {
  public override var buttonStyle: MDCButtonStyleType { .outlined }
}

// MARK: - APButtonText

public class APButtonText: APButton {
  @IBInspectable
  public var textColor: UIColor = R.color.primaryFull()! {
    didSet { didSetTitleColor() }
  }

  public override var buttonStyle: MDCButtonStyleType { .textButton }

  public override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    didSetTitleColor()
  }

  private func didSetTitleColor() {
    setTitleColor(textColor, for: .normal)
  }
}

// MARK: - APButtonBottomFull

public class APButtonBottomFull: APButton {
  public override var buttonStyle: MDCButtonStyleType { .bottomFull }
}

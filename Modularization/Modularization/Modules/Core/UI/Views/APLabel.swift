//
//  APLabel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

@IBDesignable
public class APLabelBase: UILabel {
  public var fontStyle: UIFont { T.textStyle.body }

  public override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  public override func prepareForInterfaceBuilder() {
    setup()
  }

  private func setup() {
    font = fontStyle
  }
}

public class APLabelLargeTitle: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.largeTitle }
}

public class APLabelTitle1: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.title1 }
}

public class APLabelTitle2: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.title2 }
}

public class APLabelTitle3: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.title3 }
}

public class APLabelHeadline: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.headline }
}

public class APLabelBody: APLabelBase {}

public class APLabelBodyMedium: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.bodyMedium }
}

public class APLabelCallout: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.callout }
}

public class APLabelSubHeadline: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.subHeadline }
}

public class APLabelSubHeadlineMedium: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.subHeadlineMedium }
}

public class APLabelFootnote: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.footnote }
}

public class APLabelCaption: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.caption }
}

public class APLabelCaptionSemibold: APLabelBase {
  public override var fontStyle: UIFont { T.textStyle.captionSemibold }
}

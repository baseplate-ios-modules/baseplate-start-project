//
//  CustomView.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

@IBDesignable
public class CustomView: BaseView {
  @IBInspectable public var cornerRadius: CGFloat = 0 {
    didSet {
      layer.cornerRadius = cornerRadius
    }
  }

  // Note: Used `borderColour` as getter/setter for 'borderColor'
  // with Objective-C selector 'borderColor' conflicts
  // with getter/setter for 'borderColor'
  @IBInspectable public var borderColour: UIColor = .clear {
    didSet {
      layer.borderColor = borderColour.cgColor
    }
  }

  @IBInspectable public var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  // Note: Used `borderColour` in relation to `borderColour` spelling
  @IBInspectable public var layerShadowColour: UIColor = .clear {
    didSet {
      layer.shadowColor = layerShadowColour.cgColor
    }
  }

  @IBInspectable public var layerShadowRadius: CGFloat = 0 {
    didSet {
      layer.shadowRadius = layerShadowRadius
    }
  }

  @IBInspectable public var layerShadowOpacity: Float = 0 {
    didSet {
      layer.shadowOpacity = layerShadowOpacity
    }
  }

  @IBInspectable public var layerShadowOffset: CGSize = .zero {
    didSet {
      layer.shadowOffset = layerShadowOffset
    }
  }
}

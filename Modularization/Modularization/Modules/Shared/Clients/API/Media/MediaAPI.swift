//
//  MediaAPI.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

public protocol MediaAPI {
  func postUploadMedia(
    with data: Data,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  )
}

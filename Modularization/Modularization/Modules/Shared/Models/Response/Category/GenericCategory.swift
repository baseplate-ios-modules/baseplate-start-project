//
//  GenericCategory.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct GenericCategory: APIModel, Codable, Equatable {
  public let id: Int
  public let label: String
}

//
//  Photo.Meta.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public extension Photo {
  struct Meta {
    public var id: Int? = nil
    public var data: Data? = nil
  }
}

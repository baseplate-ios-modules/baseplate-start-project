//
//  Photo.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct Photo: APIModel, Codable {
  public let id: Int
  public let url: URL
  public let thumbUrl: URL
}

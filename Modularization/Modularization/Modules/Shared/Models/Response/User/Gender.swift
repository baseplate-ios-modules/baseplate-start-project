//
//  Gender.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public enum Gender: String, Codable, Defaultable {
  case male, female, other

  public static var defaultValue: Gender {
    return .other
  }
}

//
//  User.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public struct User: APIModel, Codable, Identifiable, Equatable {
  public let id: Int
  public var primaryUsername: UsernameType = .email
  public let email: String?
  public let phoneNumber: String?
  public let fullName: String?
  public let gender: Gender?

  // Email verification status
  public let emailVerified: Bool

  // Phone verification status
  public let phoneNumberVerified: Bool

  public let avatarPermanentUrl: URL
  public let avatarPermanentThumbUrl: URL

  public let avatar: Photo?
  
  public let birthdate: String?
  public let description: String?
  
  public let createdAt: Date
  public let updatedAt: Date
  public let onboardedAt: Date?
}

public extension User {
  var hasJustSignedUp: Bool { createdAt == updatedAt }
  
  var isEmailVerified: Bool { emailVerified }
  var isPhoneNumberVerified: Bool { phoneNumberVerified }
  var hasName: Bool { !fullName.isNilOrEmpty }
  var hasAvatar: Bool { avatar != nil }

  var birthdateValue: Date? {
    guard let bd = birthdate else { return nil }
    return Constants.Formatters.birthdateFormatter.date(from: bd)
  }
  
  var avatarThumbURL: URL {
    avatar?.thumbUrl ?? avatarPermanentThumbUrl
  }
  
  var avatarURL: URL {
    avatar?.url ?? avatarPermanentUrl
  }
  
  var hasCompletedOnboarding: Bool { onboardedAt != nil }
}

// MARK: - Other Init

public extension User {
  init(user: User, avatar: Photo?) {
    self.init(
      id: user.id,
      primaryUsername: user.primaryUsername,
      email: user.email,
      phoneNumber: user.phoneNumber,
      fullName: user.fullName,
      gender: user.gender,
      emailVerified: user.emailVerified,
      phoneNumberVerified: user.phoneNumberVerified,
      avatarPermanentUrl: user.avatarPermanentUrl,
      avatarPermanentThumbUrl: user.avatarPermanentThumbUrl,
      avatar: avatar,
      birthdate: user.birthdate,
      description: user.description,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
      onboardedAt: user.onboardedAt
    )
  }
}

//
//  DynamicLinkParser.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class DynamicLinkParser: DynamicLinkParserProtocol {
  public init() {}
}

// MARK: - Methods

public extension DynamicLinkParser {
  func parseDynamicLink(from url: URL) -> Deeplink? {
    // Extract path
    let pathString = ""

    guard let path = DynamicLinkPath(rawValue: pathString) else {
      return nil
    }

    switch path {
    case .sample:
      break
    }

    return nil
  }
}

//
//  ShortcutParser.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class ShortcutParser: ShortcutParserProtocol {
  public init() {}
}

// MARK: - Methods

public extension ShortcutParser {
  func parseShortcut(from item: UIApplicationShortcutItem) -> Deeplink? {
    guard let key = ShortcutKey(rawValue: item.type) else {
      return nil
    }

    switch key {
    case .sample:
      break
    }

    return nil
  }
}

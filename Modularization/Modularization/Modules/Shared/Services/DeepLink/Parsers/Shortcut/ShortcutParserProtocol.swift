//
//  ShortcutParserProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public protocol ShortcutParserProtocol {
  func parseShortcut(from item: UIApplicationShortcutItem) -> Deeplink?
}

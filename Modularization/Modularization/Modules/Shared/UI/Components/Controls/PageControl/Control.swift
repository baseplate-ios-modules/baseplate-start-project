//
//  Control.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

public class Control: UIControl {
  public override init(frame: CGRect) {
    super.init(frame: frame)
    prepare()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)

    // Do not call `prepare` method yet. Wait until the view is
    // completely loaded from a nib archive.
  }

  public override func awakeFromNib() {
    super.awakeFromNib()
    prepare()
  }

  /// Called after the view is initialized or loaded from a nib archive.
  ///
  /// Subclasses can override this method to provide their own implementation. Don't forget to
  /// call super somewhere in the function.
  public func prepare() {
    // Do additional setups here.
  }
}

//
//  AppIconImageView.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class AppIconImageView: ImageView {
  public override func awakeFromNib() {
    super.awakeFromNib()
    onInit()
  }

  public func onInit() {
    image = App.icon
  }
}

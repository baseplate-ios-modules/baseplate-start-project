//
//  APMultilineTextField.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MDCMultilineTextField
import RxSwift

public class APMultilineTextField: MDCMultilineTextField, APTextFieldProtocol {
  public var inputController: MDCTextInputControllerBase?
  
  @IBInspectable
  public var minHeight: CGFloat = 0 {
    didSet { setupMinHeight() }
  }

  @IBInspectable
  public var charLimit: Int = 0

  public weak var charLimitLabel: UILabel? {
    didSet { bindCharLimit() }
  }

  public override init(frame: CGRect) {
    super.init(frame: frame)
    prepare()
  }

  public required init?(coder: NSCoder) {
    super.init(coder: coder)
    prepare()
  }

  public override func awakeFromNib() {
    super.awakeFromNib()
    prepare()
  }

  public func prepare() {
    textView?.delegate = self
  }
}

// MARK: - Setup

private extension APMultilineTextField {
  func setupMinHeight() {
    guard shouldSetMinHeight else { return }
    textView?.autoSetDimension(
      .height,
      toSize: minHeight,
      relation: .greaterThanOrEqual
    )
  }
}

// MARK: - Bind

private extension APMultilineTextField {
  func bindCharLimit() {
    guard let charLimitLabel = self.charLimitLabel,
      let textView = self.textView,
      shouldSetCharLimit else { return }

    let charLimitCount = Observable<Int>.just(charLimit)
    let textCount = textView.rx.text.orEmpty.map { $0.count }

    Observable
      .combineLatest(
        textCount,
        charLimitCount,
        resultSelector: { "\($0)/\($1)" }
      )
      .bind(to: charLimitLabel.rx.text)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Getters

private extension APMultilineTextField {
  var shouldSetMinHeight: Bool { minHeight > 0 }
  var shouldSetCharLimit: Bool { charLimit > 0 }
}

// MARK: - UITextViewDelegate

extension APMultilineTextField: UITextViewDelegate {
  public func textView(
    _ textView: UITextView,
    shouldChangeTextIn range: NSRange,
    replacementText text: String
  ) -> Bool {
    guard shouldSetCharLimit else { return true }
    return textView.text.count + (text.count - range.length) <= charLimit
  }
}

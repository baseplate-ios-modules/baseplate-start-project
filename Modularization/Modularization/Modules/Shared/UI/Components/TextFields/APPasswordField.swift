//
//  APPasswordField.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MaterialTextFields
import PureLayout
import UIKit

public class APPasswordField: APTextField {
  public let rightButton = UIButton(type: .custom)

  public override var isSecureTextEntry: Bool {
    willSet {
      let image = newValue ? R.image.iconEyeShow() : R.image.iconEyeHide()
      self.rightButton.setImage(image, for: .normal)
    }
  }

  public override func prepare() {
    super.prepare()

    textContentType = .password
    keyboardType = .default
    placeholder = S.password()
    autocorrectionType = .no
    isSecureTextEntry = true
    clearButtonMode = .never

    setupRightButton()
  }

  private func setupRightButton() {
    rightButton.autoSetDimensions(to: CGSize(width: 32, height: 32))
    rightButton.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)

    rightView = rightButton
    rightViewMode = .always
  }

  @IBAction
  func rightButtonTapped(_ sender: AnyObject) {
    isSecureTextEntry.toggle()
  }
}

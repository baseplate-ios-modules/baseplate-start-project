//
//  APTextField.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MDCTextField
import UIKit

public class APTextField: MDCTextField, APTextFieldProtocol {
  public var inputController: MDCTextInputControllerBase?
  
  public var isCutActionEnabled = true
  public var isCopyActionEnabled = true
  public var isPasteActionEnabled = true
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    prepare()
  }

  public required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  public override func awakeFromNib() {
    super.awakeFromNib()
    prepare()
  }

  public func prepare() {
    // no-op
  }

  public override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(cut(_:)) {
      return isCutActionEnabled
    } else if action == #selector(copy(_:)) {
      return isCopyActionEnabled
    } else if action == #selector(paste(_:)) {
      return isPasteActionEnabled
    }

    return super.canPerformAction(action, withSender: sender)
  }
}

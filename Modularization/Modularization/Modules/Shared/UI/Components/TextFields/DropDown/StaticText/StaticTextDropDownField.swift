//
//  APDropDownField.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import DropDown
import MaterialComponents.MaterialTextFields

public class StaticTextDropDownField: APTextField, APDropDownFieldProtocol {
  public var viewModel: StaticTextDropDownFieldViewModelProtocol! {
    didSet {
      viewModel.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }
  public var onSelect: VoidResult?

  @IBInspectable public var shouldAllowMultipleSelection: Bool = false {
    didSet { setupMultipleSelection() }
  }

  public private(set) var dropDown: DropDown = .init()

  public override func prepare() {
    super.prepare()

    setup()
    bind()
  }
}

// MARK: - APDropDownFieldProtocol

extension StaticTextDropDownField {
  public var dropDownFieldVM: APDropDownFieldViewModelProtocol { viewModel }
}

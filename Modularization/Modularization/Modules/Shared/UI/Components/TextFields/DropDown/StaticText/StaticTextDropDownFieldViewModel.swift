//
//  StaticTextDropDownFieldViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class StaticTextDropDownFieldViewModel: StaticTextDropDownFieldViewModelProtocol {
  public var onRefresh: VoidResult?
  public var onTextsSelect: SingleResult<[String]>?

  public private(set) var dataSource: [String] = []
  public private(set) var selectedIndices: [Int] = []

  private let staticTexts: [String]

  public init(staticTexts: [String]) {
    self.staticTexts = staticTexts
    dataSource = staticTexts
  }
}

// MARK: - Methods

public extension StaticTextDropDownFieldViewModel {
  func refresh(withQuery query: String) {
    if query == selectionText { return }

    if query.isEmpty { return reset() }

    let lowercasedQuery = query.lowercased()

    let lowercasedStaticTexts = staticTexts
      .map { $0.lowercased() }

    let filteredLowercased = lowercasedStaticTexts
      .compactMap { $0.contains(lowercasedQuery) ? $0 : nil }

    let filteredIndices = filteredLowercased
      .compactMap { lowercasedStaticTexts.firstIndex(of: $0) }

    dataSource = filteredIndices.map { staticTexts[$0] }

    onRefresh?()
  }

  func selectItems(atIndices indices: [Int]) {
    selectedIndices = indices
    onTextsSelect?(indices.map { dataSource[$0] })
  }

  func reset() {
    selectedIndices = []
    dataSource = staticTexts
    onRefresh?()
  }
}

// MARK: - Defaults

public extension StaticTextDropDownFieldViewModel {
  var selectionText: String? {
    if selectedIndices.isEmpty { return nil }
    return selectedIndices
      .map { dataSource[$0] }
      .joined(separator: ", ")
  }
}

//
//  FormTextField.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

@IBDesignable
public class FormTextField: UITextField {
  @IBInspectable public var borderWidth: CGFloat = 1 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  @IBInspectable public var activeBorderColour: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3153360445)

  @IBInspectable public var borderColour: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3153360445) {
    didSet {
      layer.borderColor = borderColour.cgColor
    }
  }

  @IBInspectable public var placeholderTextColor: UIColor = .lightGray {
    didSet {
      let placeholderConfig = [NSAttributedString.Key.foregroundColor: placeholderTextColor]
      attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: placeholderConfig)
    }
  }

  @IBInspectable public var cornerRadius: CGFloat = 4 {
    didSet {
      layer.cornerRadius = cornerRadius
    }
  }

  @IBInspectable public var insetX: CGFloat = 16
  @IBInspectable public var insetY: CGFloat = 0

  @IBInspectable public var rightIcon: UIImage? {
    didSet {
      setupRightIcon()
    }
  }

  @IBInspectable public var leftIcon: UIImage? {
    didSet {
      setupLeftIcon()
    }
  }

  public override init(frame: CGRect) {
    super.init(frame: frame)
    afterInit()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  public override func awakeFromNib() {
    super.awakeFromNib()
    afterInit()
  }

  public func afterInit() {
    font = UIFont.systemFont(ofSize: 16)
    textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5959706764)
    addTarget(self, action: #selector(textFieldBeginEditing(_:)), for: .editingDidBegin)
    addTarget(self, action: #selector(textFieldEndEditing(_:)), for: .editingDidEnd)
  }

  public override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }

  public override func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }

  public override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }
}

// MARK: - event listener

private extension FormTextField {
  func setupRightIcon() {
    guard rightIcon != nil else { return }
    rightView = UIImageView(image: rightIcon)
    rightViewMode = .always
  }

  func setupLeftIcon() {
    guard leftIcon != nil else { return }
    leftView = UIImageView(image: leftIcon)
    leftViewMode = .always
  }
}

public extension FormTextField {
  @objc func textFieldBeginEditing(_ textField: UITextField) {
    layer.borderColor = activeBorderColour.cgColor
  }

  @objc func textFieldEndEditing(_ textField: UITextField) {
    layer.borderColor = borderColour.cgColor
  }
}

//
//  PageContentController.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import PureLayout
import UIKit

public class PageContentController: ViewController {
  var viewModel: PageContentViewModelProtocol!

  @IBOutlet public private(set) var bgTopPaddingView: UIView!
  @IBOutlet public private(set) var backgroundView: UIView!
  @IBOutlet public private(set) var imageView: UIImageView!
  @IBOutlet public private(set) var titleLabel: UILabel!
  @IBOutlet public private(set) var descriptionLabel: UILabel!

  private let descriptionMargin: CGFloat = 55
  private let titleBottomPadding: CGFloat = 24

  public override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }

  private func setupView() {
    guard viewModel != nil else { return }

    titleLabel.text = viewModel.title
    titleLabel.textColor = viewModel.titleColor
    descriptionLabel.text = viewModel.description
    descriptionLabel.textColor = viewModel.descriptionColor
    descriptionLabel.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: titleBottomPadding)
    descriptionLabel.autoPinEdge(
      toSuperviewSafeArea: .left,
      withInset: descriptionMargin
    )
    descriptionLabel.autoPinEdge(
      toSuperviewSafeArea: .right,
      withInset: descriptionMargin
    )
    view.layoutIfNeeded()

    imageView.image = viewModel.image
    imageView.contentMode = viewModel.imageContentMode
    imageView.clipsToBounds = viewModel.imageClipsToBounds

    backgroundView.backgroundColor = viewModel.imageBackgroundColor
    bgTopPaddingView.backgroundColor = viewModel.imageBackgroundColor
  }
}

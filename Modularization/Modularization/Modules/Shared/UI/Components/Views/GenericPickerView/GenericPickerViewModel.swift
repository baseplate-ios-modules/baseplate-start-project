//
//  GenericPickerViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class GenericPickerViewModel: GenericPickerViewModelProtocol {
  public var onOptionsReload: VoidResult?
  public var onOptionSelect: SingleResult<String>?
  public var onOptionIndexSelect: SingleResult<Int>?

  public private(set) var options: [String]?
}

// MARK: - Methods

public extension GenericPickerViewModel {
  func setOptions(_ options: [String]) {
    self.options = options
    onOptionsReload?()
  }

  func selectOption(at index: Int) {
    onOptionIndexSelect?(index)

    guard let selectedOption = options?[index] else { return }
    onOptionSelect?(selectedOption)
  }
}

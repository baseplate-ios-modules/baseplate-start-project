//
//  GenericPickerViewModelProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public protocol GenericPickerViewModelProtocol: class {
  var onOptionsReload: VoidResult? { get set }
  var onOptionIndexSelect: SingleResult<Int>? { get set }
  var onOptionSelect: SingleResult<String>? { get set }

  var options: [String]? { get }

  func setOptions(_ options: [String])
  func selectOption(at index: Int)
}

//
//  PhotoSelectionCellViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class PhotoSelectionCellViewModel: PhotoSelectionCellViewModelProtocol {
  public private(set) var imageData: Data?
  public let imageURL: URL?
  
  private let photo: Photo?

  public init(imageData: Data) {
    self.imageData = imageData
    imageURL = nil
    photo = nil
  }

  public init(imageURL: URL) {
    self.imageURL = imageURL
    imageData = nil
    photo = nil
  }
  
  public init(photo: Photo) {
    self.photo = photo
    imageURL = photo.thumbUrl
    imageData = nil
  }
}

// MARK: - Methods

public extension PhotoSelectionCellViewModel {
  func setImageData(_ imageData: Data) {
    self.imageData = imageData
  }
}

// MARK: - Getters

public extension PhotoSelectionCellViewModel {
  var id: Int? { photo?.id }
}

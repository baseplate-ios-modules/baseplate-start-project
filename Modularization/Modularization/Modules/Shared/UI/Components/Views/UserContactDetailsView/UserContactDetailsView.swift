//
//  UserContactDetailsView.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import RxCocoa
import RxSwift

public class UserContactDetailsView: BaseView, NibLoadable {
  public var viewModel: UserContactDetailsViewModelProtocol! {
    didSet { refresh() }
  }

  @IBOutlet public private(set) var contentView: UIView!
  @IBOutlet public private(set) var imageView: UIImageView!
  @IBOutlet public private(set) var nameLabel: UILabel!
  @IBOutlet public private(set) var emailLabel: UILabel!
  @IBOutlet public private(set) var mobileNumberLabel: UILabel!

  public override func prepare() {
    loadNib()

    bind()
  }
}

// MARK: - Bind

private extension UserContactDetailsView {
  func bind() {
    emailLabel.bindHiddenIfEmptyOrNilText()
    mobileNumberLabel.bindHiddenIfEmptyOrNilText()
  }
}

// MARK: - Refresh

private extension UserContactDetailsView {
  func refresh() {
    imageView.setImageWithURL(
      viewModel.imageURL,
      placeholder: R.image.profilePicPlaceholder()
    )
    nameLabel.text = viewModel.nameText
    emailLabel.text = viewModel.emailText
    mobileNumberLabel.text = viewModel.mobileNumberText
  }
}

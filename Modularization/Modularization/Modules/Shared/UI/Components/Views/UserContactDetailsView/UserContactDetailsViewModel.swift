//
//  UserContactDetailsViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class UserContactDetailsViewModel: UserContactDetailsViewModelProtocol {
  private let user: User

  public init(user: User) {
    self.user = user
  }
}

// MARK: - Getters

public extension UserContactDetailsViewModel {
  var imageURL: URL { user.avatarPermanentThumbUrl }
  var nameText: String { user.fullName ?? "" }
  var emailText: String? { user.email }
  var mobileNumberText: String? { user.phoneNumber }
}

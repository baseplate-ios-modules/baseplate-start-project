//
//  VersionInfoView.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

public class VersionInfoView: BaseView, NibLoadable {
  public var viewModel: VersionInfoViewModelProtocol! = VersionInfoViewModel() {
    didSet { refresh() }
  }

  @IBOutlet public private(set) var contentView: UIView!
  @IBOutlet public private(set) var versionLabel: APLabelFootnote!

  public override func prepare() {
    loadNib()
    setup()
    refresh()
  }
}

// MARK: - Setup

private extension VersionInfoView {
  func setup() {
    backgroundColor = .clear
    contentView.backgroundColor = .clear
  }
}

// MARK: - Refresh

private extension VersionInfoView {
  func refresh() {
    versionLabel.text = viewModel.versionText
  }
}

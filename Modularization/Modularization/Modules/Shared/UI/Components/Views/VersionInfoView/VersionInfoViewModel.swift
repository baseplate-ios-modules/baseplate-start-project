//
//  VersionInfoViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

public class VersionInfoViewModel: VersionInfoViewModelProtocol {
}

// MARK: - Getters

public extension VersionInfoViewModel {
  var versionText: String {
    S.versionInfo(
      App.releaseVersion,
      App.buildNumber
    )
  }
}

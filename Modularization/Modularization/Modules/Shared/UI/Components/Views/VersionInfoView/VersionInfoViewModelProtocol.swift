//
//  VersionInfoViewModelProtocol.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

public protocol VersionInfoViewModelProtocol {
  var versionText: String { get }
}

//
//  MockAccessToken.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import FBSDKLoginKit

class MockAccessToken: AccessToken {
  init(mockTokenString: String = "") {
    super.init(
      tokenString: mockTokenString,
      permissions: [],
      declinedPermissions: [],
      expiredPermissions: [],
      appID: "",
      userID: "",
      expirationDate: nil,
      refreshDate: nil,
      dataAccessExpirationDate: nil
    )
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

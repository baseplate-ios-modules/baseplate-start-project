//
//  MockLoginManager.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import FBSDKLoginKit

class MockLoginManager: LoginManager {
  var mockToken: MockAccessToken! = .init()
  var errorToReturn: Error?
  var shouldCancel: Bool = false

  private(set) var logInPermissions: [String]?
  private(set) var logInFromViewController: UIViewController?
  private(set) var logInCallCount: Int = 0

  override func logIn(
    permissions: [String],
    from fromViewController: UIViewController?,
    handler: LoginManagerLoginResultBlock? = nil
  ) {
    logInCallCount += 1
    logInPermissions = permissions
    logInFromViewController = fromViewController

    if let e = errorToReturn {
      handler?(nil, e)
    } else if shouldCancel {
      handler?(MockLoginManagerLoginResult(isCancelled: true), nil)
    } else {
      handler?(MockLoginManagerLoginResult(mockToken: mockToken), nil)
    }
  }
}

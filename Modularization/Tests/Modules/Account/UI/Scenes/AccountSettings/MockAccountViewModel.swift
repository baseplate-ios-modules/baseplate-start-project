//
//  MockAccountViewModel.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import Modularization

class MockAccountViewModel: AccountViewModelProtocol {
  var profileImageURL: URL?
  var fullNameText: String?
  var usernameText: String?
  
  var isEmailPrimary: Bool = true
}

//
//  DummyVerificationToken.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import Modularization

extension VerificationToken {
  init(
    testToken: String = "",
    testExpiresAt: Date = .now
  ) {
    self.init(
      token: testToken,
      expiresAt: testExpiresAt
    )
  }
}

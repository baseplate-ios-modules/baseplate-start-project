//
//  CircularProgressViewTests.swift
//  Modularization
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import Modularization

class CircularProgressViewTests: QuickSpec {
  override func spec() {
    describe("CircularProgressView") {
      var sut: CircularProgressView!

      beforeEach {
        sut = CircularProgressView(frame: .zero)
      }

      it("should call onComplete callback once on animate") {
        var onCompleteCallCount = 0
        sut.animate(
          duration: 0.01,
          onComplete: { onCompleteCallCount += 1 }
        )
        
        expect(onCompleteCallCount).toEventually(equal(1))
      }
    }
  }
}

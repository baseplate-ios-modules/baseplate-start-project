//
//  Button+Extensions.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
  func tap() {
    sendActions(for: .touchUpInside)
  }
}

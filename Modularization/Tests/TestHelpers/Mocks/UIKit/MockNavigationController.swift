//
//  MockNavigationController.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class MockNavigationController: UINavigationController {
  private(set) var pushViewControllerCallCount: Int = 0
  private(set) var pushedViewController: UIViewController!

  private(set) var popToRootViewControllerCallCount: Int = 0

  private(set) var popToViewControllerCallCount: Int = 0
  private(set) var poppedToViewController: UIViewController!
  
  private(set) var setNavigationBarHiddenCallCount: Int = 0
  private(set) var setNavigationBarHiddenValue: Bool?
  
  private(set) var presentCallCount: Int = 0
  private(set) var presentViewController: UIViewController?

  override func pushViewController(_ viewController: UIViewController, animated: Bool) {
    super.pushViewController(viewController, animated: false)
    pushViewControllerCallCount += 1
    pushedViewController = viewController
  }

  override func popToRootViewController(animated: Bool) -> [UIViewController]? {
    let vcs = super.popToRootViewController(animated: false)
    popToRootViewControllerCallCount += 1
    return vcs
  }

  override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
    let vcs = super.popToViewController(viewController, animated: false)
    popToViewControllerCallCount += 1
    poppedToViewController = viewController
    return vcs
  }

  override func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
    super.setNavigationBarHidden(hidden, animated: false)
    setNavigationBarHiddenCallCount += 1
    setNavigationBarHiddenValue = hidden
  }
  
  override func present(
    _ viewControllerToPresent: UIViewController,
    animated flag: Bool,
    completion: (() -> Void)? = nil
  ) {
    super.present(viewControllerToPresent, animated: false, completion: completion)
    presentCallCount += 1
    presentViewController = viewControllerToPresent
  }
}
